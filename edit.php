<?php
include('config.php');
if ($protect) {
    require_once('protect.php');
}
error_reporting(E_ERROR);
?>

<html lang="en">
<!-- Author: Dmitri Popov, dmpop@linux.com
         License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->

<head>
	<title>RSSplendid</title>
	<meta charset="utf-8">
	<link rel="shortcut icon" href="favicon.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/css/uikit.min.css" />
	<script src="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/js/uikit.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/js/uikit-icons.min.js"></script>
	<style>
		textarea {
			font-size: 15px;
			width: 100%;
			height: 55%;
			line-height: 1.9;
			margin-top: 2em;
		}
	</style>
</head>

<body>
	<div class="uk-container uk-margin-top">
		<?php
		if ($_POST["save"]) {
			Write();
		};
		?>
		<h1>Edit RSS feeds</h1>
		<?php
		function Read()
		{
			$CONFIGFILE = "feeds.txt";
			echo file_get_contents($CONFIGFILE);
		}
		function Write()
		{
			$CONFIGFILE = "feeds.txt";
			$fp = fopen($CONFIGFILE, "w");
			$data = $_POST["text"];
			fwrite($fp, $data);
			fclose($fp);
			echo "<script>";
				echo "UIkit.notification({message: 'Changes saved.'});";
				echo "</script>";
		}
		?>
		<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
			<textarea name="text"><?php Read(); ?></textarea>
			<input class="uk-button uk-button-primary uk-margin-top" type="submit" name="save" value="Save">
			<a class="uk-button uk-button-default uk-margin-top" href="index.php">Back</a>
		</form>
	</div>
</body>

</html>