<!DOCTYPE html>
<html lang="en">

<!-- Author: Dmitri Popov, dmpop@linux.com
	 License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->

<head>
	<title>RSSplendid</title>
	<meta charset="utf-8">
	<link rel="shortcut icon" href="favicon.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/css/uikit.min.css" />
	<script src="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/js/uikit.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/js/uikit-icons.min.js"></script>
</head>

<body>
	<div class="uk-container uk-margin-small-top">
		<div class="uk-card uk-card-primary uk-card-body">
			<h1 class="uk-heading-line uk-text-center"><span>RSSplendid</span></h1>
			<form action=" " method="POST">
				<p>Select RSS feed: </p>
				<select class="uk-select" name="rss">
					<?php
					$feed_list = fopen("feeds.txt", "r");
					while (!feof($feed_list)) {
						$feed = str_replace(array("\n", "\r"), '', fgets($feed_list));
						echo "<option value='$feed'>$feed</option>";
					}
					fclose($feed_list);
					?>
				</select>
				<input class="uk-button uk-button-primary uk-margin-top" type="submit" value="Show feed" name="show">
				<a class="uk-button uk-button-default uk-margin-top" href="edit.php">Edit</a>
			</form>
		</div>
			<?php
			if (isset($_POST['show'])) {
				$rss = simplexml_load_file($_POST['rss']);
				echo '<h3>' . $rss->channel->title . '</h3>';
				foreach ($rss->channel->item as $item) {
					echo "<div class='uk-container uk-margin-small-top'>";
					echo "<div class='uk-card uk-card-default uk-card-body'>";
					echo '<h3><a href="' . $item->link . '">' . $item->title . "</a></h3>";
					echo "<p>" . $item->description . "</p>";
					echo "</div>";
					echo "</div>";
				}
			}
			?>
	</div>
</body>

</html>