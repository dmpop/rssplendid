FROM php:7.4-cli
LABEL maintainer="dmpop@linux.com"
LABEL version="0.1"
LABEL description="RSSplendid Docker image"
COPY . /usr/src/rssplendid
WORKDIR /usr/src/rssplendid
EXPOSE 8000
CMD [ "php", "-S", "0.0.0.0:8000" ]